package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceA implements InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceA.class);

    @Autowired
    private ServiceB mainServiceB;

    @Autowired
    private ServiceB secondaryServiceB;

    @Override
    public void afterPropertiesSet() throws Exception {
        LOGGER.info(mainServiceB.toString());
        LOGGER.info(secondaryServiceB.toString());
        LOGGER.info(String.valueOf(mainServiceB.equals(secondaryServiceB)));
    }
}
